## Description

Bayesian regularized artificial neural network for gap filling.

## Install

```
pip install -U git+https://gitlab.com/rbraswell/banng.git
python -c "from banng.test.test import test; test()" 
```

## Contents

* nnregress.py (Bayesian nonlinear regression)
* nngapfill.py (Inference-based gap filling using nnregress.py)
* netlab_lib.py (Netlab library functions ported to python)

## NNgapfill

Use Bayesian regularlized artificial neural netword regression to fill gaps in tabular data in an iterative scheme. The scheme leverages statistical multivariate linear or nonlinear relationships between the columns. On the first pass, each column is filled using all the other columns that have valid data in the rows that have gaps in the column of interest. On the second pass, the original gaps in each column are filled using all the other columns. Columns with all gaps are not used. Some utilities are provided to assist gapfilling observations over time.

## NNregress

Perform Bayesian nonlinear regression using an artificial neural network. The regression generally selects the model with minimum curvature necessary to describe the data, and thus represents a good solution to the problem of overfitting in artificial neural networks. The function is called and used just a like a multivariate regression in matlab or python, except than both the X and Y variables can have multiple dimensions.
A choice of normal and exponential error distributions is provided.

## History

* 2011-08-16 Converted to Python
* 2016-07-21 Array copy bug fixes
* 2017-03-09 License applied for FOSS release
* 2018-02-28 Repackaged using setuptools

NNREGRESS INCLUDES CODE ADAPTED FROM THE NETLAB LIBRARY BY IAN NABNEY, PORTED TO PYTHON AND MODIFIED BY BOBBY BRASWELL TO BE USED FOR APPLICATION (SEE REFERENCES BELOW)

MODIFICATIONS INCLUDE: CHOICE OF ERROR DISTRIBUTION AND MAXIMUM LIKELIHOOD WEIGHTING

SEE NETLAB LICENSE FILE: netlab_lib_lic.txt

SEE ALSO NETLAB SITE: http://www.aston.ac.uk/eas/research/groups/ncrg/resources/netlab/downloads

## References

(a) Bishop C (1995) Neural networks for pattern recognition. Oxford University Press, 502pp.

(a) MacKay DJC (1992) A practical Bayesian framework for backpropagation networks. Neural
    Computation, 4, 448-472.

(b) Nabney, I (2001) NETLAB: Algorithms for pattern recognition. Springer, 420pp.

(c) Braswell BH, Sacks B, Linder E, Schimel DS (2005) Estimating ecosystem process parameters by
    assimilation of eddy flux observations of NEE. Global Change Biology, 11:335-355.

(c) Braswell BH, Hagen SC, Frolking SE, Salas WA (2005) A multivariable approach for mapping sub-
    pixel land cover distributions using MISR and MODIS: An application in the Brazilian Amazon.
    Remote Sensing of Environment, 87:243-256.

(c) Hagen SC, Braswell BH, Linder E, Frolking SE, Richardson AD, Hollinger DY (2006) Statistical
    uncertainty of eddy flux-based estimates of gross ecosystem carbon exchange at Howland Forest,
    Maine, Journal of Geophysical Research, 111:D08S03

(c) Moffat, A.M., D. Papale, M. Reichstein, A.G. Barr, B.H. Braswell, G. Churkina, A.R. Desai, E.
    Falge, J.H. Gove, M. Heimann, D.Y. Hollinger, D. Hui, A.J.  Jarvis, J. Kattge, A. Noormets, 
    A.D. Richardson, V.J. Stauch. Comprehensive comparison of gap filling techniques for net carbon
    fluxes. Agricultural and Forest Meteorology, 147:209-232, 2007.

*(a) publications on the theory of Bayesian regularized neural nets; (b) book containing matlab code which we used and modified; (c) papers containing our applications of the method*

## Example

Also see test cases included.

```
    np.random.seed(7)
    npts = 120
    slope1 = -0.5
    slope2 = 2.5
    slope3 = 1.0
    b1 = 20.
    b2 = 5.
    eps1 = 0.5 
    eps2 = 0.3 
    x = 10 + np.random.randn(npts, 2)
    t = eps1*np.random.randn(npts, 2)
    t[:,0] = t[:,0] + b1 + slope1*x[:,0] + slope2*x[:,1]
    t[:,1] = t[:,1] - b2 + slope3*x[:,0] + slope1*x[:,1]
    w = np.ones((npts, 1))
    y, net, sig = nnregress(x, t, nhidden=5, nstd=2, diag=1, disttype='n', mlpwgts=w)    
    y2, sig2 = nnfwd(net, x)
    assert (y - y2).mean() == 0.
    print "np.abs(t[1:,0] - y[1:,0]).mean()", np.abs(t[1:,0] - y[1:,0]).mean()
    print "sig.mean()", sig.mean()
    plot(t[:,0], y[:,0], 'plot1.pdf')
    plot(t[:,1], y[:,1], 'plot2.pdf')

```