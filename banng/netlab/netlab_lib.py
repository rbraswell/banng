import sys
import numpy as np
import copy

""" 
Functions ported to python from the Netlab matlab library

http://www.aston.ac.uk/eas/research/groups/ncrg/resources/netlab

These functions were taken from subset of the Netlab source
as of 2011-08-16

SEE NETLAB LICENSE FILE IN THIS DIRECTORY
netlab_lib_lic.txt

Individual function comment strings preserved without change
"""

def errbayes(net, edata):
    """
    ERRBAYES Evaluate Bayesian error function for network.    
    Description
    E = ERRBAYES(NET, EDATA) takes a network data structure  NET together
    the data contribution to the error for a set of inputs and targets.
    It returns the regularised error using any zero mean Gaussian priors
    on the weights defined in NET.
    [E, EDATA, EPRIOR] = ERRBAYES(NET, X, T) additionally returns the
    data and prior components of the error.
    See also
    GLMERR, MLPERR, RBFERR
    Copyright (c) Ian T Nabney (1996-2001)
    """
    e1 = net.beta*edata
    w = netpak(net)
    eprior = 0.5*np.dot(w, w)
    e2 = eprior*net.alpha
    e = e1 + e2
    return e, edata, eprior

def evidence(net, x, t, num=None):
    """
    EVIDENCE Re-estimate hyperparameters using evidence approximation.
    Description
    [NET] = EVIDENCE(NET, X, T) re-estimates the hyperparameters ALPHA
    and BETA by applying Bayesian re-estimation formulae for NUM
    iterations. The hyperparameter ALPHA can be a simple scalar
    associated with an isotropic prior on the weights, or can be a vector
    in which each component is associated with a group of weights as
    defined by the INDEX matrix in the NET data structure. These more
    complex priors can be set up for an MLP using MLPPRIOR. Initial
    values for the iterative re-estimation are taken from the network
    data structure NET passed as an input argument, while the return
    argument NET contains the re-estimated values.
    [NET, GAMMA, LOGEV] = EVIDENCE(NET, X, T, NUM) allows the re-
    estimation  formula to be applied for NUM cycles in which the re-
    estimated values for the hyperparameters from each cycle are used to
    re-evaluate the Hessian matrix for the next cycle.  The return value
    GAMMA is the number of well-determined parameters and LOGEV is the
    log of the evidence.
    See also
    MLPPRIOR, NETGRAD, NETHESS, DEMEV1, DEMARD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    ndata = x.shape[0]
    if not num:
        num = 1
    # Extract weights from network
    w = netpak(net)
    h, dh = nethess(w, net, x, t)
    # THIS USED TO BE
    #evl, evec = np.linalg.eig(dh)
    # NOW ASSUMES SYMMETRIC DH
    evl = np.linalg.eigvalsh(dh)
    #print 'evl'
    #print evl
    # Now set the negative eigenvalues to zero
    evl = evl*(evl > 0)
    # safe_evl is used to avoid taking log of zero
    safe_evl = evl + sys.float_info.epsilon*(evl <= 0)
    e, edata, eprior = neterr(w, net, x, t)
    # Do the re-estimation
    for k in range(num):
        # Re-estimate alpha
        L = evl
        L = net.beta*L
        gamma = np.sum(L/(L + net.alpha))
        net.alpha = 0.5*gamma/eprior
        # Partially evaluate log evidence: only include unmasked weights
        logev = 0.5*len(w)*np.log(net.alpha)
        # Re-estimate beta
        net.beta = 0.5*(net.nout*ndata - gamma)/edata        
        logev = logev + 0.5*ndata*np.log(net.beta) - 0.5*ndata*np.log(2*np.pi)
        local_beta = net.beta
        # Evaluate new log evidence
        e = errbayes(net, edata)
        logev = logev - e - 0.5*np.sum(np.log(local_beta*safe_evl + net.alpha))
    return net, gamma

def gbayes(net, gdata):
    """
    GBAYES	Evaluate gradient of Bayesian error function for network.
    Description
    G = GBAYES(NET, GDATA) takes a network data structure NET together
    the data contribution to the error gradient for a set of inputs and
    targets. It returns the regularised error gradient using any zero
    mean Gaussian priors on the weights defined in NET.  In addition, if
    a MASK is defined in NET, then the entries in G that correspond to
    weights with a 0 in the mask are removed.
    [G, GDATA, GPRIOR] = GBAYES(NET, GDATA) additionally returns the data
    and prior components of the error.
    See also
    ERRBAYES, GLMGRAD, MLPGRAD, RBFGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    g1 = gdata*net.beta
    # Evaluate the prior contribution to the gradient
    w = netpak(net)
    gprior = w
    g2 = net.alpha*gprior
    g = g1 + g2
    return g, gdata, gprior

def hbayes(net, hdata):
    """
    HBAYES	Evaluate Hessian of Bayesian error function for network.
    Description
    H = HBAYES(NET, HDATA) takes a network data structure NET together
    the data contribution to the Hessian for a set of inputs and targets.
    It returns the regularised Hessian using any zero mean Gaussian
    priors on the weights defined in NET.  In addition, if a MASK is
    defined in NET, then the entries in H that correspond to weights with
    a 0 in the mask are removed.
    [H, HDATA] = HBAYES(NET, HDATA) additionally returns the data
    component of the Hessian.
    See also
    GBAYES, GLMHESS, MLPHESS, RBFHESS
    Copyright (c) Ian T Nabney (1996-2001)
    """
    nwts = net.nwts
    h = net.beta*hdata
    h += net.alpha*np.eye(nwts)
    return h, hdata

def mlpbkp(net, x, z, deltas):
    """
    MLPBKP	Backpropagate gradient of error function for 2-layer network.
    Description
    G = MLPBKP(NET, X, Z, DELTAS) takes a network data structure NET
    together with a matrix X of input vectors, a matrix  Z of hidden unit
    activations, and a matrix DELTAS of the  gradient of the error
    function with respect to the values of the output units (i.e. the
    summed inputs to the output units, before the activation function is
    applied). The return value is the gradient G of the error function
    with respect to the network weights. Each row of X corresponds to one
    input vector.    
    This function is provided so that the common backpropagation
    algorithm can be used by multi-layer perceptron network models to
    compute gradients for mixture density networks as well as standard
    error functions.
    See also
    MLP, MLPGRAD, MLPDERIV, MDNGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    # Evaluate second-layer gradients
    gw2 = np.dot(z.T, deltas)
    gb2 = deltas.sum(0)
    # Now do the backpropagation
    delhid = np.dot(deltas, net.w2.T)
    delhid = delhid*(1.0 - z**2)
    # Finally, evaluate the first-layer gradients.
    gw1 = np.dot(x.T, delhid)
    gb1 = delhid.sum(0)
    g = np.hstack((gw1.T.ravel(), gb1, gw2.T.ravel(), gb2))
    return g

def mlpderiv(net, x):
    """
    MLPDERIV Evaluate derivatives of network outputs with respect to weights.
    Description
    G = MLPDERIV(NET, X) takes a network data structure NET and a matrix
    of input vectors X and returns a three-index matrix G whose I, J, K
    element contains the derivative of network output K with respect to
    weight or bias parameter J for input pattern I. The ordering of the
    weight and bias parameters is defined by MLPUNPAK.
    See also
    MLP, MLPPAK, MLPGRAD, MLPBKP
    Copyright (c) Ian T Nabney (1996-2001)
    """
    y, z, _a = mlpfwd(net, x)
    ndata = x.shape[0]
    nwts = net.nwts
    g = np.zeros((ndata, nwts, net.nout))
    for k in range(net.nout):
        delta = np.zeros(net.nout)
        delta[k] = 1
        for n in range(ndata):
            xv = x[n,:].reshape(1, net.nin)
            zv = z[n,:].reshape(1, net.nhidden)
            dv = delta.reshape(1, net.nout)
            g[n, :, k] = mlpbkp(net, xv, zv, dv)
    return g

def mlperr(net, x, t):
    """
    MLPERR	Evaluate error function for 2-layer network.
    Description
    E = MLPERR(NET, X, T) takes a network data structure NET together
    with a matrix X of input vectors and a matrix T of target vectors,
    and evaluates the error function E. The choice of error function
    corresponds to the output unit activation function. Each row of X
    corresponds to one input vector and each row of T corresponds to one
    target vector.
    [E, EDATA, EPRIOR] = MLPERR(NET, X, T) additionally returns the data
    and prior components of the error, assuming a zero mean Gaussian
    prior on the weights with inverse variance parameters ALPHA and BETA
    taken from the network data structure NET.
    See also
    MLP, MLPPAK, MLPUNPAK, MLPFWD, MLPBKP, MLPGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    y, z, a = mlpfwd(net, x)        
    if net.mlpwgts.shape != x.shape:
        wts = 1
    else:
        wts = net.mlpwgts
    if net.outfn == 'linear':
        if net.disttype == 'n':
            edata = 0.5*((y - t)**2/wts).sum()
        elif net.disttype == 'e':
            edata = (np.abs(y - t)/wts).sum()
    elif net.outfn =='logistic':
        maxcut = -np.log(sys.float_info_epsilon)
        mincut = -np.log(1.0/sys.float_info.min - 1.0)
        a = a.clip(mincut, maxcut)
        y = 1.0/(1.0 + np.exp(-a))
        edata = -(t*log(y) + (1.0 - t)*log(1.0 - y)).sum()
    elif net.outfn == 'softmax':
        maxcut = np.log(sys.float_info.max) - np.log(net.nout)
        mincut = np.log(sys.float_info.min)
        a = a.clip(mincut, maxcut)
        temp = np.exp(a)
        y = temp / (temp.sum(1)*np.ones((net.nout, 1))).T
        y[y < sys.float_info.min] = sys.float_info.min
        edata = -(t*log(y)).sum()
    else:
        raise Exception, 'Unknown activation function %s' % net.outfn
    # passes edata through
    e, edata, eprior = errbayes(net, edata)
    return e, edata, eprior

def mlpfwd(net, x):
    """
    MLPFWD	Forward propagation through 2-layer network.
    Description
    Y = MLPFWD(NET, X) takes a network data structure NET together with a
    matrix X of input vectors, and forward propagates the inputs through
    the network to generate a matrix Y of output vectors. Each row of X
    corresponds to one input vector and each row of Y corresponds to one
    output vector.    
    [Y, Z] = MLPFWD(NET, X) also generates a matrix Z of the hidden unit
    activations where each row corresponds to one pattern.
    [Y, Z, A] = MLPFWD(NET, X) also returns a matrix A  giving the summed
    inputs to each output unit, where each row corresponds to one
    pattern.
    See also
    MLP, MLPPAK, MLPUNPAK, MLPERR, MLPBKP, MLPGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    ndata = x.shape[0]
    z = np.tanh(np.dot(x, net.w1) + np.ones((ndata, net.nhidden))*net.b1)
    a = np.dot(z, net.w2) + np.ones((ndata, net.nout))*net.b2
    if net.outfn == 'linear':
        y = a
    elif net.outfn == 'logistic':
        maxcut = -np.log(sys.float_info.epsilon)
        # Ensure that log(y) is computable
        mincut = -np.log(1.0/sys.float_info.min - 1.0)
        a = a.clip(mincut, maxcut)
        y = 1.0/(1 + np.exp(-a))
    elif net.outfn == 'softmax':
        # Prevent overflow and underflow: use same bounds as glmerr
        # Ensure that sum(exp(a), 2) does not overflow
        maxcut = np.log(sys.float_info.max) - np.log(net.nout)
        # Ensure that exp(a) > 0
        mincut = np.log(sys.float_info.min)
        a = a.clip(mincut, maxcut)
        temp = np.exp(a)
        # Elementwise arithmetic
        y = temp / (temp.sum(1)*np.ones((net.nout, 1))).T
    else:
        raise Exception, 'Unknown activation function %s' % net.outfn
    return y, z, a

def mlpgrad(net, x, t):
    """
    MLPGRAD Evaluate gradient of error function for 2-layer network.
    Description
    G = MLPGRAD(NET, X, T) takes a network data structure NET  together
    with a matrix X of input vectors and a matrix T of target vectors,
    and evaluates the gradient G of the error function with respect to
    the network weights. The error funcion corresponds to the choice of
    output unit activation function. Each row of X corresponds to one
    input vector and each row of T corresponds to one target vector.    
    [G, GDATA, GPRIOR] = MLPGRAD(NET, X, T) also returns separately  the
    data and prior contributions to the gradient. In the case of multiple
    groups in the prior, GPRIOR is a matrix with a row for each group and
    a column for each weight parameter.
    See also
    MLP, MLPPAK, MLPUNPAK, MLPFWD, MLPERR, MLPBKP
    Copyright (c) Ian T Nabney (1996-2001)
    """
    if net.mlpwgts.shape != x.shape:
        wts = 1
    else:
        wts = net.mlpwgts
    y, z, a = mlpfwd(net, x)
    if net.disttype == 'n':
        delout = (y - t)/wts
    elif net.disttype == 'e':
        delout = np.sign(y - t)/wts
    gdata = mlpbkp(net, x, z, delout)
    g, gdata, gprior = gbayes(net, gdata)
    return g

def mlphdotv(net, x, t, v):
    """
    MLPHDOTV Evaluate the product of the data Hessian with a vector. 
    Description
    HDV = MLPHDOTV(NET, X, T, V) takes an MLP network data structure NET,
    together with the matrix X of input vectors, the matrix T of target
    vectors and an arbitrary row vector V whose length equals the number
    of parameters in the network, and returns the product of the data-
    dependent contribution to the Hessian matrix with V. The
    implementation is based on the R-propagation algorithm of
    Pearlmutter.
    See also
    MLP, MLPHESS, HESSCHEK
    Copyright (c) Ian T Nabney (1996-2001)
    """
    ndata = x.shape[0]
    # Standard forward propagation
    y, z, _a = mlpfwd(net, x)
    # Hidden unit first derivatives
    zprime = 1.0 - z*z
    # Hidden unit second derivatives
    zpprime = -2.0*z*zprime
    # unpack the v vector
    vnet = mlpunpak(net, v)
    # Do the R-forward propagation
    ra1 = np.dot(x, vnet.w1) + np.ones((ndata, 1))*vnet.b1
    rz = zprime*ra1
    ra2 = np.dot(rz, net.w2) + np.dot(z, vnet.w2) + np.ones((ndata, 1))*vnet.b2
    if net.outfn == 'linear':
        ry = ra2
    elif net.outfn == 'logistic':
        ry = y*(1.0 - y)*ra2
    elif net.outfn == 'softmax':
        nout = t.shape[0]
        ry = y*ra2 - y*((y*ra2).sum(1)*np.ones((1, nout)))
    else:
        raise Exception, 'Unknown activation function %s' % net.outfn
    # Evaluate delta for the output units
    delout = y - t
    # Do the standard backpropagation
    delhid = zprime*np.dot(delout, net.w2.T)
    # Now do the R-backpropagation
    rdelhid = zpprime*ra1*np.dot(delout, net.w2.T) + zprime*np.dot(delout, vnet.w2.T) \
        + zprime*np.dot(ry, net.w2.T)
    # Finally, evaluate the components of hdv and then merge into long vector
    hw1 = np.dot(x.T, rdelhid)    
    hb1 = rdelhid.sum(0)
    # Finish
    hw2 = np.dot(z.T, ry) + np.dot(rz.T, delout)
    hb2 = ry.sum(0)
    hdv = np.hstack((hw1.T.ravel(), hb1, hw2.T.ravel(), hb2))
    return hdv

def mlphess(net, x, t, hdata=None):
    """
    #MLPHESS Evaluate the Hessian matrix for a multi-layer perceptron network.
    Description
    H = MLPHESS(NET, X, T) takes an MLP network data structure NET, a
    matrix X of input values, and a matrix T of target values and returns
    the full Hessian matrix H corresponding to the second derivatives of
    the negative log posterior distribution, evaluated for the current
    weight and bias values as defined by NET.
    [H, HDATA] = MLPHESS(NET, X, T) returns both the Hessian matrix H and
    the contribution HDATA arising from the data dependent term in the
    Hessian.
    H = MLPHESS(NET, X, T, HDATA) takes a network data structure NET, a
    matrix X of input values, and a matrix T of  target values, together
    with the contribution HDATA arising from the data dependent term in
    the Hessian, and returns the full Hessian matrix H corresponding to
    the second derivatives of the negative log posterior distribution.
    This version saves computation time if HDATA has already been
    evaluated for the current weight and bias values.
    See also
    MLP, HESSCHEK, MLPHDOTV, EVIDENCE
    Copyright (c) Ian T Nabney (1996-2001)
    """
    if not hdata:
        # Data term in Hessian needs to be computed
        hdata = datahess(net, x, t)
    h, hdata = hbayes(net, hdata)
    return h, hdata

def datahess(net, x, t):
    """
    Compute data part of Hessian
    """
    hdata = np.zeros((net.nwts, net.nwts))
    for v in np.eye(net.nwts):
        hdata[np.where(v),:] = mlphdotv(net, x, t, v)
    return hdata

class mlp(object):
    """
    MLP	Create a 2-layer feedforward network.
    Description
    NET = MLP(NIN, NHIDDEN, NOUT, FUNC) takes the number of inputs,
    hidden units and output units for a 2-layer feed-forward network,
    together with a string FUNC which specifies the output unit
    activation function, and returns a data structure NET. The weights
    are drawn from a zero mean, unit variance isotropic Gaussian, with
    varianced scaled by the fan-in of the hidden or output units as
    appropriate. This makes use of the Matlab function RANDN and so the
    seed for the random weight initialization can be  set using
    RANDN('STATE', S) where S is the seed value.  The hidden units use
    the TANH activation function.    
    The fields in NET are
    type = 'mlp'
    nin = number of inputs
    nhidden = number of hidden units
    nout = number of outputs
    nwts = total number of weights and biases
    actfn = string describing the output unit activation function:
    'linear'
    'logistic
    'softmax'
    w1 = first-layer weight matrix
    b1 = first-layer bias vector
    w2 = second-layer weight matrix
    b2 = second-layer bias vector
    Here W1 has dimensions NIN times NHIDDEN, B1 has dimensions 1 times
    NHIDDEN, W2 has dimensions NHIDDEN times NOUT, and B2 has dimensions
    1 times NOUT.
    NET = MLP(NIN, NHIDDEN, NOUT, FUNC, PRIOR), in which PRIOR is a
    scalar, allows the field NET.ALPHA in the data structure NET to be
    set, corresponding to a zero-mean isotropic Gaussian prior with
    inverse variance with value PRIOR. Alternatively, PRIOR can consist
    of a data structure with fields ALPHA and INDEX, allowing individual
    Gaussian priors to be set over groups of weights in the network. Here
    ALPHA is a column vector in which each element corresponds to a
    separate group of weights, which need not be mutually exclusive.  The
    membership of the groups is defined by the matrix INDX in which the
    columns correspond to the elements of ALPHA. Each column has one
    element for each weight in the matrix, in the order defined by the
    function MLPPAK, and each element is 1 or 0 according to whether the
    weight is a member of the corresponding group or not. A utility
    function MLPPRIOR is provided to help in setting up the PRIOR data
    structure.
    NET = MLP(NIN, NHIDDEN, NOUT, FUNC, PRIOR, BETA) also sets the
    additional field NET.BETA in the data structure NET, where beta
    corresponds to the inverse noise variance.    
    See also
    MLPPRIOR, MLPPAK, MLPUNPAK, MLPFWD, MLPERR, MLPBKP, MLPGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    def __init__(self, nin, nhidden, nout, outfunc, prior, beta):
        self.type = 'mlp'
        self.nin = nin
        self.nhidden = nhidden
        self.nout = nout
        self.nwts = (nin + 1)*nhidden + (nhidden + 1)*nout
        outfns = ('linear', 'logistic', 'softmax')
        if outfunc not in outfns:
            raise Exception, "Undefined output function. Exiting."
        self.outfn = outfunc
        self.alpha = prior
        self.w1 = np.random.randn(nin, nhidden)/np.sqrt(nin + 1.0)
        self.b1 = np.random.randn(nhidden)/np.sqrt(nin + 1.0)
        self.w2 = np.random.randn(nhidden, nout)/np.sqrt(nhidden + 1.0)
        self.b2 = np.random.randn(nout)/np.sqrt(nhidden + 1.0)
        self.beta = beta
        return

    def __repr__(self):
        outstr = ''
        outstr += 'type=' + self.type + '\n'
        outstr += 'nin=' + str(self.nin) + '\n'
        outstr += 'nhidden=' + str(self.nhidden) + '\n'
        outstr += 'nout=' + str(self.nout) + '\n'
        outstr += 'nwts=' + str(self.nwts) + '\n'
        outstr += 'outfn=' + str(self.outfn) + '\n'
        outstr += 'alpha=' + str(self.alpha) + '\n'
        outstr += 'w1=' + str(self.w1) + '\n'
        outstr += 'b1=' + str(self.b1) + '\n'
        outstr += 'w2=' + str(self.w2) + '\n'
        outstr += 'b2=' + str(self.b2) + '\n'
        outstr += 'beta=' + str(self.beta) + '\n'
        outstr += 'disttype=' + self.disttype
        return outstr

def mlppak(net):
    """
    MLPPAK	Combines weights and biases into one weights vector.
    Description
    W = MLPPAK(NET) takes a network data structure NET and combines the
    component weight matrices bias vectors into a single row vector W.
    The facility to switch between these two representations for the
    network parameters is useful, for example, in training a network by
    error function minimization, since a single vector of parameters can
    be handled by general-purpose optimization routines.    
    The ordering of the paramters in W is defined by
    w = [net.w1(:)', net.b1, net.w2(:)', net.b2];
    where W1 is the first-layer weight matrix, B1 is the first-layer
    bias vector, W2 is the second-layer weight matrix, and B2 is the
    second-layer bias vector.
    See also
    MLP, MLPUNPAK, MLPFWD, MLPERR, MLPBKP, MLPGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    wa = net.w1.T.ravel()
    wb = net.b1
    wc = net.w2.T.ravel()
    wd = net.b2
    w = np.hstack((wa, wb, wc, wd))
    return w

def mlpunpak(net, w):
    """
    MLPUNPAK Separates weights vector into weight and bias matrices.     
    Description
    NET = MLPUNPAK(NET, W) takes an mlp network data structure NET and  a
    weight vector W, and returns a network data structure identical to
    the input network, except that the first-layer weight matrix W1, the
    first-layer bias vector B1, the second-layer weight matrix W2 and the
    second-layer bias vector B2 have all been set to the corresponding
    elements of W.
    See also
    MLP, MLPPAK, MLPFWD, MLPERR, MLPBKP, MLPGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """    
    if net.nwts != len(w):
        raise Exception, 'Invalid weight vector length'
    nin = net.nin
    nhidden = net.nhidden
    nout = net.nout
    mark1 = nin*nhidden
    # make a shallow copy
    thisnet = copy.copy(net)
    thisnet.w1 = w[:mark1].reshape(nhidden, nin).T
    mark2 = mark1 + nhidden
    thisnet.b1 = w[mark1:mark2].reshape(nhidden)
    mark3 = mark2 + nhidden*nout
    thisnet.w2 = w[mark2:mark3].reshape(nout, nhidden).T
    mark4 = mark3 + nout
    thisnet.b2 = w[mark3:mark4].reshape(nout)
    return thisnet

def neterr(w, net, x, t):
    """
    NETERR Evaluate network error function for generic optimizers    
    Description
    E = NETERR(W, NET, X, T) takes a weight vector W and a network data
    structure NET, together with the matrix X of input vectors and the
    matrix T of target vectors, and returns the value of the error
    function evaluated at W.    
    [E, VARARGOUT] = NETERR(W, NET, X, T) also returns any additional
    return values from the error function.
    See also
    NETGRAD, NETHESS, NETOPT
    Copyright (c) Ian T Nabney (1996-2001)
    """
    net = netunpak(net, w)
    e, edata, eprior = mlperr(net, x, t)
    return e, edata, eprior

def netunpak(net, w):
    """
    NETUNPAK Separates weights vector into weight and bias matrices.     
    Description
    NET = NETUNPAK(NET, W) takes an net network data structure NET and  a
    weight vector W, and returns a network data structure identical to
    the input network, except that the componenet weight matrices have
    all been set to the corresponding elements of W.  If there is  a MASK
    field in the NET data structure, then the weights in W are placed in
    locations corresponding to non-zero entries in the mask (so W should
    have the same length as the number of non-zero entries in the MASK).    
    See also
    NETPAK, NETFWD, NETERR, NETGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    net = mlpunpak(net, w)
    return net

def netgrad(w, net, x, t):
    """
    NETGRAD Evaluate network error gradient for generic optimizers
    Description
    G = NETGRAD(W, NET, X, T) takes a weight vector W and a network data
    structure NET, together with the matrix X of input vectors and the
    matrix T of target vectors, and returns the gradient of the error
    function evaluated at W.
    See also
    MLP, NETERR, NETOPT
    Copyright (c) Ian T Nabney (1996-2001)
    """
    net = netunpak(net, w)
    g = mlpgrad(net, x, t)
    return g

def nethess(w, net, x, t):
    """
    NETHESS Evaluate network Hessian
    Description    
    H = NETHESS(W, NET, X, T) takes a weight vector W and a network data
    structure NET, together with the matrix X of input vectors and the
    matrix T of target vectors, and returns the value of the Hessian
    evaluated at W.
    [E, VARARGOUT] = NETHESS(W, NET, X, T, VARARGIN) also returns any
    additional return values from the network Hessian function, and
    passes additional arguments to that function.
    See also
    NETERR, NETGRAD, NETOPT
    Copyright (c) Ian T Nabney (1996-2001)
    """
    net = netunpak(net, w)
    h, dh = mlphess(net, x, t)
    return h, dh

def netopt(net, options, x, t, alg):
    """
    NETOPT	Optimize the weights in a network model. 
    Description
    NETOPT is a helper function which facilitates the training of
    networks using the general purpose optimizers as well as sampling
    from the posterior distribution of parameters using general purpose
    Markov chain Monte Carlo sampling algorithms. It can be used with any
    function that searches in parameter space using error and gradient
    functions.    
    [NET, OPTIONS] = NETOPT(NET, OPTIONS, X, T, ALG) takes a network
    data structure NET, together with a vector OPTIONS of parameters
    governing the behaviour of the optimization algorithm, a matrix X of
    input vectors and a matrix T of target vectors, and returns the
    trained network as well as an updated OPTIONS vector. The string ALG
    determines which optimization algorithm (CONJGRAD, QUASINEW, SCG,
    etc.) or Monte Carlo algorithm (such as HMC) will be used.
    [NET, OPTIONS, VARARGOUT] = NETOPT(NET, OPTIONS, X, T, ALG) also
    returns any additional return values from the optimisation algorithm.
    See also
    NETGRAD, BFGS, CONJGRAD, GRADDESC, HMC, SCG
    Copyright (c) Ian T Nabney (1996-2001)
    """
    # Extract weights from network as single vector
    w = netpak(net)
    # Carry out optimisation
    w, options, err = scg('neterr', w, options, 'netgrad', net, x, t)
    # Pack the weights back into the network
    net = netunpak(net, w)
    return net, options, err

def netpak(net):
    """
    NETPAK	Combines weights and biases into one weights vector.
    Description
    W = NETPAK(NET) takes a network data structure NET and combines the
    component weight matrices  into a single row vector W. The facility
    to switch between these two representations for the network
    parameters is useful, for example, in training a network by error
    function minimization, since a single vector of parameters can be
    handled by general-purpose optimization routines.  This function also
    takes into account a MASK defined as a field in NET by removing any
    weights that correspond to entries of 0 in the mask.    
    See also
    NET, NETUNPAK, NETFWD, NETERR, NETGRAD
    Copyright (c) Ian T Nabney (1996-2001)
    """
    w = mlppak(net)
    return w

def scg(f, x, options, gradf, net, xnet, tnet):
    """
    SCG	Scaled conjugate gradient optimization.
    Description
    [X, OPTIONS] = SCG(F, X, OPTIONS, GRADF) uses a scaled conjugate
    gradients algorithm to find a local minimum of the function F(X)
    whose gradient is given by GRADF(X).  Here X is a row vector and F
    returns a scalar value. The point at which F has a local minimum is
    returned as X.  The function value at that point is returned in
    OPTIONS(8).    
    [X, OPTIONS, FLOG, POINTLOG, SCALELOG] = SCG(F, X, OPTIONS, GRADF)
    also returns (optionally) a log of the function values after each
    cycle in FLOG, a log of the points visited in POINTLOG, and a log of
    the scale values in the algorithm in SCALELOG.
    SCG(F, X, OPTIONS, GRADF, P1, P2, ...) allows additional arguments to
    be passed to F() and GRADF().     The optional parameters have the
    following interpretations.    
    OPTIONS(1) is set to 1 to display error values; also logs error
    values in the return argument ERRLOG, and the points visited in the
    return argument POINTSLOG.  If OPTIONS(1) is set to 0, then only
    warning messages are displayed.  If OPTIONS(1) is -1, then nothing is
    displayed.
    OPTIONS(2) is a measure of the absolute precision required for the
    value of X at the solution.  If the absolute difference between the
    values of X between two successive steps is less than OPTIONS(2),
    then this condition is satisfied.    
    OPTIONS(3) is a measure of the precision required of the objective
    function at the solution.  If the absolute difference between the
    objective function values between two successive steps is less than
    OPTIONS(3), then this condition is satisfied. Both this and the
    previous condition must be satisfied for termination.
    OPTIONS(9) is set to 1 to check the user defined gradient function.
    OPTIONS(10) returns the total number of function evaluations
    (including those in any line searches).
    OPTIONS(11) returns the total number of gradient evaluations.    
    OPTIONS(14) is the maximum number of iterations; default 100.
    See also
    CONJGRAD, QUASINEW
    Copyright (c) Ian T Nabney (1996-2001)
    """
    #  Set up the options
    if len(options) < 18:
        raise Exception, 'Options vector too short'
    if options[14]:
        niters = options[14]
    else:
        niters = 100
    display = options[1]
    gradcheck = options[9]
    nparams = len(x)
    sigma0 = 1.0e-4
    # Initial error
    fold = neterr(x, net, xnet, tnet)[0]
    fnow = fold
    # Increment function evaluation counter
    options[10] += 1
    gradnew = netgrad(x, net, xnet, tnet)
    gradold = gradnew
    # Increment gradient evaluation counter
    options[11] += 1
    # Initial search direction
    d = -gradnew
    # Force calculation of directional derivs
    success = 1
    # nsuccess counts number of successes
    nsuccess = 0
    # Initial scale parameter
    beta = 1.0
    # Lower bound on scale
    betamin = 1.0e-15
    # Upper bound on scale.
    betamax = 1.0e100
    # j counts number of iterations
    j = 1
    # Main optimization loop
    while j <= niters:
        if success == 1:
            mu = np.dot(d, gradnew)
            if mu >= 0.0:
                d = -gradnew
                mu = np.dot(d, gradnew.T)
            kappa = np.dot(d, d.T)
            if kappa < sys.float_info.epsilon:
                options[8] = fnow
                return x, options, fnow
            sigma = sigma0/np.sqrt(kappa)
            xplus = x + sigma*d
            gplus = netgrad(xplus, net, xnet, tnet)
            options[11] += 1
            theta = np.dot(d, gplus - gradnew)/sigma
        # Increase effective curvature and evaluate step size alpha
        delta = theta + beta*kappa
        if delta <= 0.0:
            delta = beta*kappa
            beta -= theta/kappa
        alpha = -mu/delta
        # Calculate the comparison ratio
        xnew = x + alpha*d
        fnew = neterr(xnew, net, xnet, tnet)[0]
        options[10] += 1
        Delta = 2.0*(fnew - fold)/(alpha*mu)
        if Delta >= 0.0:
            success = 1
            nsuccess += 1
            x = xnew
            fnow = fnew
        else:
            success = 0
            fnow = fold
        if success == 1:
            # test for termination
            if np.max(np.abs(alpha*d)) < options[2] and np.abs(fnew-fold) < options[3]:
                options[8] = fnew
                return x, options, fnew
            else:
                # Update variables for new position
                fold = fnew
                gradold = gradnew
                gradnew = netgrad(x, net, xnet, tnet)
                options[11] += 1
                # If the gradient is zero then we are done
                if np.dot(gradnew, gradnew.T) == 0.0:
                    options[8] = fnew
                    return x, options, fnew
                # end if
            # endif
        # end if
        # adjust beta according to comparison ratio
        if Delta < 0.25:
            beta = min([4.0*beta, betamax])
        if Delta > 0.75:
            beta = max([0.5*beta, betamin])
        # Update search direction using Polak-Ribiere formula, or re-start 
        # in direction of negative gradient after nparams steps
        if nsuccess == nparams:
            d = -gradnew
            nsuccess = 0
        else:
            if success == 1:
                gamma = np.dot((gradold - gradnew), gradnew.T)/mu
                d = gamma*d - gradnew    
            # end if
        j = j + 1
    # end while
    # If we get here, then we haven't terminated in the given number of 
    # iterations
    options[8] = fold
    if options[1] >= 0:
        raise Exception, "Too many iterations"
    return x, options, fold
