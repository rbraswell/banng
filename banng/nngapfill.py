import os
import sys
import logging

import numpy as np
import pandas as pd
from banng.nnregress import nnregress, nnfwd


class NoGapException(Exception):
    pass

class AllGapException(Exception):
    pass


def assess_missing(data, missingval):
    nrow, ncol = data.shape
    count = np.zeros(ncol, dtype='int32')
    for icol in range(ncol):
        if np.std(data[:,icol]) == 0.0:
            count[icol] = nrow
        else:
            count[icol] = len(np.where(data[:,icol] == missingval)[0])
    return count


def nngapfill(data, missingval, targetcols=None, nhidden=2, thresh=0.5,
              ignore=[], names=None, quiet=False, makemask=None, disttype='n'):
    logging.basicConfig(level=quiet*logging.FATAL)
    log = logging.getLogger(__name__)

    assert makemask in (None, 'level', 'error')
    assert disttype in ('n', 'e')

    missing = assess_missing(data, missingval)
    nrow, ncol = data.shape
    col_nogaps = np.where(missing == 0)[0]
    col_allgaps = np.where(missing > thresh*nrow)[0]
    col_somegaps = np.where((missing > 0)&(missing <= thresh*nrow))[0]
    col_valid = np.where((missing >= 0)&(missing <= thresh*nrow))[0]
    col_mostgaps = np.where((missing < nrow)&(missing > thresh*nrow))[0]

    log.debug('missing, %s' % missing)
    log.debug('col_nogaps %s' % col_nogaps)
    log.debug('col_allgaps %s' % col_allgaps)
    log.debug('col_somegaps %s' % col_somegaps)
    log.debug('col_valid %s' % col_valid)
    log.debug('col_mostgaps %s' % col_mostgaps)

    assert len(col_nogaps) > 0, "must have at least 1 column with no gaps"
    assert len(col_somegaps) > 0, "must have at least 1 column with gaps. try raising the threshold"

    if targetcols is None:
        targetcols = col_somegaps
    elif len(targetcols) > 0:
        if set(targetcols).issubset(col_nogaps):
            raise NoGapException
        if not set(targetcols).isdisjoint(col_allgaps):
            raise AllGapException
        if not set(targetcols).isdisjoint(col_nogaps):
            log.info(['Some target columns have no gaps',\
                list(set(targetcols).intersection(col_nogaps))])
            targetcols = set(targetcols).difference(col_nogaps)

    if names is not None:
        assert len(names) == ncol
    else:
        names = [str(i) for i in range(ncol)]

    def do_fill(col, othercols, indata, outdata):
        wnogap = np.where(data[:,col] != missingval)[0]
        wgap = np.where(data[:,col] == missingval)[0]
        x = indata[np.ix_(wnogap, othercols)]
        t = indata[np.ix_(wnogap, [col])]
        # remove columns with std = 0
        stds = x.std(axis=0)
        if np.any(stds == 0):
            log.info('removing bad column')
            badcols = np.where(stds == 0)[0]
            badcols = np.array(othercols)[badcols].tolist()
            othercols = list(set(othercols).difference(badcols))
            x = indata[np.ix_(wnogap, othercols)]
        try:
            y, net, sig = nnregress(x, t, nhidden, nstd=2, diag=0, disttype='n')
            x = indata[np.ix_(wgap, othercols)]
            y, sig = nnfwd(net, x)
            log.debug(['sig', sig.mean(), np.std(t), sig.mean()/np.std(t), net.r2[0]])
            outdata[wgap, col] = y.flatten()
            err = sig.squeeze()/np.std(t)
        except:
            log.info('nnregress failed, using mean %f' %  t.mean())
            outdata[wgap, col] = t.mean()
            err = 0.0*t.mean()
        return outdata, err

    # first pass over all columns with some gaps
    mask = np.zeros_like(data)
    newdata1 = data.copy()
    newdata2 = data.copy()
    for col in col_somegaps:
        log.debug(['filling (first pass)', col, names[col]])
        othercols = list(set(col_nogaps).difference(ignore))
        log.debug(['othercols', othercols])
        newdata2, sig = do_fill(col, othercols, newdata1, newdata2)
        w = (newdata2[:,col] != missingval)&(newdata1[:,col] == missingval)
        if makemask == "error":
            mask[w,col] = sig
        elif makemask == "level":
            mask[w,col] = 1

    # second pass over target columns only
    newdata1 = newdata2.copy()
    for col in targetcols:
        if not np.any(data[:,col] == missingval):
            continue
        log.debug(['filling (second pass)', col, names[col]])
        othercols = list(set(col_valid).difference([col], ignore))
        log.debug(['othercols', othercols])
        newdata2, sig = do_fill(col, othercols, newdata1, newdata2)
        w = (newdata2[:,col] != missingval)&(data[:,col] == missingval)
        if makemask == "error":
            mask[w,col] = sig
        elif makemask == "level":
            mask[w,col] = 2

    # cleanup pass over data with more than thresh fraction of gaps
    if len(col_mostgaps) > 0:
        newdata1 = newdata2.copy()
        for col in col_mostgaps:
            log.debug(['filling (third pass)', col, names[col]])
            othercols = list(set(col_valid).difference(ignore))
            log.debug(['othercols', othercols])
            newdata2, sig = do_fill(col, othercols, newdata1, newdata2)
            w = (newdata2[:,col] != missingval)&(newdata1[:,col] == missingval)
            if makemask == "error":
                mask[w,col] = sig
            elif makemask == "level":
                mask[w,col] = 3

    if makemask is not None:
        log.debug([(mask==i).mean() for i in range(4)])
        return newdata2, mask
    else:
        return newdata2

# TODO: make this work
def run_from_csv(filepath, missingval):
    table = pd.read_csv(filepath)
    data = table.as_matix()
    nngapfill(data, missingval=missingval)
