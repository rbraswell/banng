import numpy as np
from banng.netlab.netlab_lib import *

def nnregress(xin, tin, nhidden=2, nstd=2, diag=1, disttype='n', mlpwgts=[]):
    """ execute main regression routine """
    # Initial prior hyperparameter
    alpha_init = 0.005
    # Initial noise hyperparameter
    beta_init = 100.
    # Max number of outer loops
    nouter = 500
    # Number of inner loops
    ninner = 3
    # When to exit outer loop
    toler = [.005, .005, .005, .005]
    # Default options vector
    # options = zeros(1,18)
    options = np.zeros(18)
    # Absolute precision for weights
    options[2] = 1.0e-7
    # Precision for error function
    options[3] = 1.0e-7
    # Number of cycles in inner loop
    options[14] = 100
    # Suppress warning messages
    options[1] = -1
    # dimensions
    x = xin.copy()
    t = tin.copy()
    nx, nin = x.shape
    try:
        nt, nout = t.shape
    except:
        raise Exception, "Target data must be nrow x ncol"
    if nt != nx:
        raise Exception, "Incompatible inputs: nx != nt"
    # columnwise stats
    meanx = x.mean(axis=0)
    stdx = x.std(axis=0, ddof=1)
    meant = t.mean(axis=0)
    stdt = t.std(axis=0, ddof=1)
    if (stdx==0).sum() | (stdt==0).sum():
        wx = np.where(stdx==0)[0]
        wt = np.where(stdt==0)[0]
        raise Exception, "Bad inputs: std==0, %s, %s" % (repr(wx), repr(wt))
    # normalize inputs and outputs
    for i in range(nin):
        x[:,i] = (x[:,i] - meanx[i])/(nstd*stdx[i])
    for i in range(nout):
        t[:,i] = (t[:,i] - meant[i])/(nstd*stdt[i])
    # Create and initialize network weight vector.
    net = mlp(nin, nhidden, nout, 'linear', alpha_init, beta_init)
    net.disttype = disttype
    # this is either [] or an array the length of the data set
    net.mlpwgts = np.array(mlpwgts)
    nweights = net.nwts
    if diag == 1:
        print nweights
    # Train using scaled conjugate gradients, re-estimating alpha and beta.
    state = np.ones((1,4))
    for k in range(nouter):
        net, options, err = netopt(net, options, x, t, 'scg')
        net, gamma = evidence(net, x, t, ninner)
        if net.alpha<1.E-30 or net.beta<1.E-30:
            raise Exception, "No evidence"
        if np.isnan(net.alpha) or np.isnan(net.beta) or np.isnan(gamma) or net.alpha > 900.0:
            raise Exception, "No evidence"
        thisstate = np.array([net.alpha, net.beta, gamma, err])
        state = np.vstack((state, thisstate))
        satisfy = abs(state[-1] - state[-2])/state.mean(0) < toler
        err = err/nx
        if diag == 1:
            print 'Cycle=%d alpha=%f beta=%f gamma=%f error=%f satisfy=%s' %\
                  (k, net.alpha, net.beta, gamma, err, str(satisfy))
        if np.sum(satisfy) == 4:
            if diag == 1:
                print 'Satisfied'
            break
    if k == nouter:
        print 'Warning: maximum outer loop iterations reached'
    if diag == 1:
        print 'number of iterations', k
    # Forward estimation
    y = mlpfwd(net, x)[0]
    # Evaluate error bars
    hess = mlphess(net, x, t)[0]
    invhess = np.linalg.inv(hess)
    g = mlpderiv(net, x)
    sig = np.zeros((nx, nout))
    for k in range(nout):
        for n in range(nx):
            grad = g[n,:,k]
            sig[n,k] = np.dot(grad, np.dot(invhess, grad.T))
    arg = np.ones_like(sig)/net.beta + sig
    if np.isnan(sig).any():
        raise Exception, "Unable to compute sigma at rows %s" % repr(list(np.where(arg < 0.0)[0]))
    else:
        sig = np.sqrt(arg)
    net.invhess = invhess
    net.hess = hess
    # Return to original units
    for i in range(nout):
        t[:,i] = t[:,i]*(nstd*stdt[i]) + meant[i]
        y[:,i] = y[:,i]*(nstd*stdt[i]) + meant[i]
        sig[:,i] = sig[:,i]*(nstd*stdt[i])
    rsq = np.zeros(nout)
    for i in range(nout):
        r = np.corrcoef(t[:,i], y[:,i])[0,1]
        rsq[i] = r*r
        if (diag == 1):
            print 'R^2: %6.4f' % rsq[i]
	net.r2 = rsq
    #Save the configuration of this regression
    net.opt_nstd = nstd
    net.opt_alpha_init = alpha_init
    net.opt_beta_init = beta_init
    net.opt_nouter = nouter
    net.opt_ninner_evi = ninner
    net.opt_ninner_opt = options[14]
    net.opt_meant = meant
    net.opt_meanx = meanx
    net.opt_stdt = stdt
    net.opt_stdx = stdx
    net.opt_minx = x.min()
    net.opt_maxx = x.max()
    return y, net, sig

def nnfwd(net, xin, normalize=True):
    """ wrap mlpfwd() """
    if np.array(net.r2).sum() == 0:
        raise Exception, "Bad network"
    nin = net.nin
    nout = net.nout
    nstd = net.opt_nstd
    meant = net.opt_meant
    stdt = net.opt_stdt
    meanx = net.opt_meanx
    stdx = net.opt_stdx
    x = xin.copy()
    nx, nin_in = x.shape
    assert nin == nin_in
    # normalize inputs and outputs
    for i in range(nin):
        x[:,i] = (x[:,i] - meanx[i])/(nstd*stdx[i])
    y = mlpfwd(net, x)[0]
    # Evaluate error bars
    invhess = net.invhess
    g = mlpderiv(net, x)
    sig = np.zeros((nx, nout))
    for k in range(nout):
        for n in range(nx):
            grad = g[n,:,k]
            sig[n,k] = np.dot(grad, np.dot(invhess, grad.T))
    arg = np.ones_like(sig)/net.beta + sig
    if np.isnan(sig).any():
        #raise Exception, "Unable to compute sigma at rows %s" % repr(list(np.where(arg < 0.0)[0]))
        print "Unable to compute sigma at rows %s" % repr(list(np.where(arg < 0.0)[0]))
    else:
        sig = np.sqrt(arg)
    # Return to original units
    for i in range(nout):
        y[:,i] = y[:,i]*(nstd*stdt[i]) + meant[i]
        sig[:,i] = sig[:,i]*(nstd*stdt[i])
    return y, sig


def nnregress_file(infile, xvars, yvars):
    import pandas as pd
    table = pd.read_csv(infile)
    n = len(table)
    x = table[xvars].as_matrix().reshape(n, len(xvars))
    t = table[yvars].as_matrix().reshape(n, len(yvars))
    y, net, sig = nnregress(x, t, nhidden=2, nstd=1, diag=1, disttype='n', mlpwgts=[])
    return y, net.r2
