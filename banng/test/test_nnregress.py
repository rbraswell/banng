import os
import numpy as np
from banng.nnregress import nnregress_file

TESTFILE = os.path.join(os.path.dirname(__file__), 'testdata.csv')

def test_nnregress1():
    np.random.seed(333)
    y, r2 = nnregress_file(TESTFILE, ['LE', 'FC'], ['NEE'])
    print r2[0], round(r2[0], 4), 0.8864
    assert round(r2[0], 4) == 0.8864

def test_nnregress2():
    np.random.seed(333)
    y, r2 = nnregress_file(TESTFILE, ['LE', 'FC', 'DOY', 'HRMIN'], ['NEE', 'LE'])
    print round(r2[0], 4), round(r2[1], 4)
    assert (round(r2[0], 4) == 0.8878) & (round(r2[1], 4) == 0.9996)
