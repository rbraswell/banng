import os
import numpy as np
import pandas as pd
from banng.nngapfill import nngapfill
from banng.modules.harmonic import harmonic

TESTFILE = os.path.join(os.path.dirname(__file__), 'testdata.csv')

def test_nngapfill1():
    np.random.seed(333)
    table = pd.read_csv(TESTFILE)
    data = table.as_matrix()
    sinday, cosday = harmonic(data[:,0], 365.25)
    data = np.hstack((data, sinday, cosday))
    data_orig = data.copy()
    data[20:30, 0] = -9999.
    data[30:40, 4] = -9999.
    data[40:50, 7] = -9999.
    data[10:300, 9] = -9999.
    data_fill, mask = nngapfill(data, missingval=-9999., makemask='level')
    err1 = np.sqrt(((data_fill[20:30, 0] - data_orig[20:30, 0])**2).mean())
    err2 = np.sqrt(((data_fill[30:40, 4] - data_orig[30:40, 4])**2).mean())
    err3 = np.sqrt(((data_fill[40:50, 7] - data_orig[40:50, 7])**2).mean())
    err4 = np.sqrt(((data_fill[10:300, 9] - data_orig[10:300, 9])**2).mean())
    assert round(err1, 5) == 0.04570
    assert round(err2, 5) == 3.90612
    assert round(err3, 5) == 0.69263
    assert round(err4, 5) == 0.17676
    assert mask.sum() == 930
