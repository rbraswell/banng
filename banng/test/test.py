#!/usr/bin/env python

from banng.test import test_nngapfill, test_nnregress

from pdb import set_trace

def test():
    testfuns = [
        test_nngapfill.test_nngapfill1,
        test_nnregress.test_nnregress1,
        test_nnregress.test_nnregress2,
    ]
    nfail = 0
    npass = 0
    for testfun in testfuns:
        try:
            testfun()
        except:
            nfail += 1
        else:
            npass += 1
    print "pass={} fail={}".format(npass, nfail)

if __name__ == '__main__':
    test()
