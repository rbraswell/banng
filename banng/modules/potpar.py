from math import *
from datetime import datetime, timedelta
import calendar
import numpy as np

def datetime2args(dt):
    args = {'year': dt.year,
            'doy': int(dt.strftime('%j')),
            'hour': dt.hour,
            'minute': dt.minute}
    return args

def potpar(lon, lat, dts):
    par = []
    for dt in dts:
        par.append(potpar_inst(lon, lat, **datetime2args(dt)))
    return par

def potpar_inst(lon, lat, year, doy, hour, minute):
    # fraction direct sun
    sunFrac = 1.0
    # Location [rad]
    lonRad = radians(lon)
    latRad = radians(lat)
    # earth rotation frequency [per hour] and day angle [rad]
    omega = 2.*pi/24.
    dayAng = 2.*pi*doy/365.
    # orbital eccentricity [dimensionless]
    eccent = 1.000110 + 0.034221*cos(dayAng) + 0.00128*sin(dayAng)\
             + 0.000719*cos(2*dayAng) + 0.000077*sin(2*dayAng)
    # declination of the ecliptic [rad]
    # the coefficients are "declination coefficients"
    decl = (0.006918 - 0.399912*cos(dayAng) + 0.070257*sin(dayAng)\
            - 0.006758*cos(2*dayAng) + 0.000907*sin(2*dayAng)\
            - 0.002697*cos(3*dayAng) + 0.00148*sin(3*dayAng))
    localTime = hour + minute/60.
    hourAng = 2.*pi*(localTime)/24. - pi
    # cosine of the solar zenith angle
    cosSza = sin(latRad)*sin(decl)\
             + cos(latRad)*cos(decl)*cos(omega*localTime - pi)
    # solar zenith angle [rad]
    solarZen = acos(cosSza)
    # total insolation [W m-2]
    dayness = 1367*eccent*(cos(decl)*cos(latRad)*cos(omega*localTime - pi)\
            + sin(decl)*sin(latRad))
    rad = max([0.0, dayness])
    # PAR, which is scaled insolation [uE m-2 s-1]
    # why not par = parFrac*rad ?
    # the theoretical expression below results in a Qp/Rs (parFrac) of >1.64
    # observations suggest a range of 1.9-2.2
    par = (0.18 + (.62*sunFrac))*((rad*0.001*2.05*10000000.)/10000.)
    return par

def potpar_year(lon, lat, year):
    # fraction direct sun
    sunFrac = 1.0
    # Location [rad]
    lonRad = radians(lon)
    latRad = radians(lat)
    # earth rotation frequency [per hour]
    omega = 2.*pi/24.
    # days in year
    ndays = 365 + int(calendar.isleap(year))
    par = np.zeros(ndays*48)
    for iday in range(ndays):
        # Day angle [rad]
        dayAng = 2.*pi*iday/365.
        # orbital eccentricity [dimensionless]
        eccent = 1.000110 + 0.034221*cos(dayAng) + 0.00128*sin(dayAng)\
            + 0.000719*cos(2*dayAng) + 0.000077*sin(2*dayAng)
        # declination of the ecliptic [rad]
        # the coefficients are "declination coefficients"
        decl = (0.006918 - 0.399912*cos(dayAng) + 0.070257*sin(dayAng)\
                    - 0.006758*cos(2*dayAng) + 0.000907*sin(2*dayAng)\
                    - 0.002697*cos(3*dayAng) + 0.00148*sin(3*dayAng))
        for ihhour in range(48):
            localTime = ihhour/2.0
            # Hour angle [rad]
            hourAng = 2.*pi*(localTime)/24. - pi
            # cosine of the solar zenith angle
            cosSza = sin(latRad)*sin(decl)\
                + cos(latRad)*cos(decl)*cos(omega*localTime - pi)
            # solar zenith angle [rad]
            solarZen = acos(cosSza)
            # total insolation [W m-2]
            dayness = 1367*eccent*(cos(decl)*cos(latRad)*cos(omega*localTime - pi)\
                    + sin(decl)*sin(latRad))
            rad = max([0.0, dayness])
            # PAR, which is scaled insolation [uE m-2 s-1]
            # why not par = parFrac*rad ?
            # the theoretical expression below results in a Qp/Rs (parFrac) of >1.64
            # observations suggest a range of 1.9-2.2
            count = iday*48 + ihhour
            par[count] = (.18 + (.62*sunFrac))*((rad*0.001*2.05*10000000.)/10000.)
    return par

def potpar_dtimes(lon, lat, year, dtimes):
    # fraction direct sun
    sunFrac = 1.0
    # Location [rad]
    lonRad = radians(lon)
    latRad = radians(lat)
    # earth rotation frequency [per hour]
    omega = 2.*pi/24.
    par = []
    for dtime in dtimes:
        iday = int(dtime)
        # Day angle [rad]
        dayAng = 2.*pi*iday/365.
        # orbital eccentricity [dimensionless]
        eccent = 1.000110 + 0.034221*cos(dayAng) + 0.00128*sin(dayAng)\
                + 0.000719*cos(2*dayAng) + 0.000077*sin(2*dayAng)
        # declination of the ecliptic [rad]
        # the coefficients are "declination coefficients"
        decl = (0.006918 - 0.399912*cos(dayAng) + 0.070257*sin(dayAng)\
                - 0.006758*cos(2*dayAng) + 0.000907*sin(2*dayAng)\
                - 0.002697*cos(3*dayAng) + 0.00148*sin(3*dayAng))
        localTime = 24*(dtime - iday)
        # Hour angle [rad]
        hourAng = 2.*pi*(localTime)/24. - pi
        # cosine of the solar zenith angle
        cosSza = sin(latRad)*sin(decl)\
            + cos(latRad)*cos(decl)*cos(omega*localTime - pi)
        # solar zenith angle [rad]
        solarZen = acos(cosSza)
        # total insolation [W m-2]
        dayness = 1367*eccent*(cos(decl)*cos(latRad)*cos(omega*localTime - pi)\
                + sin(decl)*sin(latRad))
        rad = max([0.0, dayness])
        # PAR, which is scaled insolation [uE m-2 s-1]
        # why not par = parFrac*rad ?
        # the theoretical expression below results in a Qp/Rs (parFrac) of >1.64
        # observations suggest a range of 1.9-2.2
        thispar = (.18 + (.62*sunFrac))*((rad*0.001*2.05*10000000.)/10000.)
        par.append(thispar)
    par = np.array(par)
    return par

# TODO: this would be used daily sums
def rebin(a, *args):
    '''rebin ndarray data into a smaller ndarray of the same rank whose dimensions
    are factors of the original dimensions. eg. An array with 6 columns and 4 rows
    can be reduced to have 6,3,2 or 1 columns and 4,2 or 1 rows.
    example usages:
    >>> a=rand(6,4); b=rebin(a,3,2)
    >>> a=rand(6); b=rebin(a,2)
    '''
    shape = a.shape
    lenShape = len(shape)
    factor = np.asarray(shape)/np.asarray(args)
    evList = ['a.reshape('] + \
             ['args[%d],factor[%d],'%(i,i) for i in range(lenShape)] + \
             [')'] + ['.sum(%d)'%(i+1) for i in range(lenShape)] + \
             ['/factor[%d]'%i for i in range(lenShape)]
    print ''.join(evList)
    return eval(''.join(evList))

def test_rebin():
    a = np.array([1,2,3,1,2,3,1,2,3,1,5,3])
    print rebin(a, a.size/3)*3
    # [6 6 6 9]

if __name__ == "__main__":
    #import matplotlib as mpl
    #mpl.use('Agg')
    from matplotlib import pyplot as plt
    from dateutil.rrule import rrule, HOURLY, MINUTELY

    lon, lat = (-105.75, 40.25)
    #lon, lat = (0., 0.)

    startdt = datetime(2004,1,14,0,0)
    enddt = datetime(2007,11,20,23,30)

    n = 2*365*48
    dts = list(rrule(MINUTELY, interval=30, count=n, dtstart=startdt))

    par = potpar(lon, lat, dts)
    print par

    plt.plot(par)
    plt.show()
    #plt.savefig('par.png')


    """
    #dt = datetime.datetime(2001,7,20,10,30)
    print potpar_(lon, lat, 2001, 7, 20, 10, 0)

    year = 2001.

    par = potpar(lon, lat, year)
    #par = (par*30*60)/1.E6 # E m-2 (total in each half hour)
    #par = rebin(par, par.size/48.)*48  # sum for each day

    print (par==0.).sum()
    print (par>0.).sum()

    plt.plot(par)
    plt.show()
    """

    """
    lon = -124.
    lat = 45.
    doy = 128.
    par = potpar(lon, lat, doy)
    print par
    """
