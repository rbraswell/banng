from setuptools import setup, find_packages

setup(
    name='banng',
    version='0.1',
    description='Bayesian artificial neural network regression for gap filling',
    url='git@gitlab.com:rbraswell/banng.git',
    author='Bobby H. Braswell',
    author_email='rbraswell@ags.io',
    classifiers=[
        'Programming Language :: Python :: 2.7',
    ],
    license='GPLv2',
    packages=find_packages(),
    install_requires=[
        'pandas',
    ],
    entry_points={
        'console_scripts': [
            'nnregress=banng.nnregress:main',
            'nngapfill=banng.nngapfill:main',
        ],
    },
    zip_safe=False,
    setup_requires=['pytest-runner'],
    tests_require=['pytest','pandas'],
    package_data={
        'banng.test': ['*.csv'],
    },
)
